package se.experis;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgotirhmTest {

    @Test
    void expectWrongCreditCardDigitTest(){
        LuhnAlgotirhm test = new LuhnAlgotirhm();
        assertEquals(false,test.validate("4485757823761196"));
        assertEquals(false,test.validate("4539676672491827"));
        assertEquals(false,test.validate("2221009421969814"));
        assertEquals(false,test.validate("5564786158918107"));
        assertEquals(false,test.validate("2221009421969819"));
        assertEquals(false,test.validate("2221001777283720"));
    }

    @Test
    void validate() {
        LuhnAlgotirhm test = new LuhnAlgotirhm();
        assertEquals(true,test.validate("4485757823761193"));
        assertEquals(true,test.validate("4539676672491825"));
        assertEquals(true,test.validate("2221009421969817"));
        assertEquals(true,test.validate("5564786158918103"));
        assertEquals(true,test.validate("2221009421969817"));
        assertEquals(true,test.validate("2221001777283729"));
    }

    @Test
    void validateCreditCardDigit() {
        LuhnAlgotirhm test = new LuhnAlgotirhm();
        assertEquals(5,test.validateCreditCardDigit("4539676672491825"));
        assertEquals(7,test.validateCreditCardDigit("2221009421969817"));
        assertEquals(3,test.validateCreditCardDigit("5564786158918103"));
        assertEquals(7,test.validateCreditCardDigit("2221009421969817"));
        assertEquals(9,test.validateCreditCardDigit("2221001777283729"));
    }

    @Test
    void validateCreditCardNumber() {
        LuhnAlgotirhm test = new LuhnAlgotirhm();
        assertEquals(true,test.validateCreditCardNumber("4539676672491825"));
        assertEquals(true,test.validateCreditCardNumber("2221009421969817"));
        assertEquals(true,test.validateCreditCardNumber("5564786158918103"));
        assertEquals(true,test.validateCreditCardNumber("2221009421969817"));
        assertEquals(true,test.validateCreditCardNumber("2221001777283729"));
    }
    @Test
    void validateCreditCardNumberWithWrongInputWithDifferentLengths() {
        LuhnAlgotirhm test = new LuhnAlgotirhm();
        assertEquals(false,test.validateCreditCardNumber("123451"));
        assertEquals(false,test.validateCreditCardNumber("236342625"));
        assertEquals(false,test.validateCreditCardNumber("215231"));
        assertEquals(false,test.validateCreditCardNumber("215231623"));
        assertEquals(false,test.validateCreditCardNumber("3246"));
    }
    @Test
    void validateCreditCardDigitWithDifferentLengths() {
        LuhnAlgotirhm test = new LuhnAlgotirhm();
        assertEquals(7,test.validateCreditCardDigit("1234517"));
        assertEquals(1,test.validateCreditCardDigit("2363426251"));
        assertEquals(2,test.validateCreditCardDigit("2152312"));
        assertEquals(4,test.validateCreditCardDigit("2152316234"));
        assertEquals(6,test.validateCreditCardDigit("32466"));
    }
}