import com.sun.security.jgss.GSSUtil;
import se.experis.LuhnAlgotirhm;

import java.util.Scanner;
/*
* This program will take in a number and will perform the Luhn Algorithm on it
* If the length on the input string is 16 and the algorithm validate the last digit the program will print out
* that the input string is a valid credit card.
* If the input string is valid but the lenght is not 16 the program will only print out that it is a valid number
* otherwise the program will print out that the string is invalid.
 */

public class Program {
    private static String inputStringWithOutLastDigit;
    private static boolean isCreditCard = true;
    private static String inputString;
    private static String lastDigit;
    private static Scanner sc;
    public static void main(String[] args) {
        inputStringMenu();

    }
    public static void validCreditCardInputString() {
        LuhnAlgotirhm validateInputString = new LuhnAlgotirhm();
        System.out.println("Input " + inputStringWithOutLastDigit + " " +lastDigit);
        System.out.println("Provided: " + validateInputString.getInputLastDigit());
        System.out.println("exp. last digit: "+ validateInputString.getExpectedLastDigit());
        System.out.println("Checksum: " +"Valid");
        System.out.println("Digits: "+ inputString.length() + " " +"(Credit card)");
        System.out.println();
        inputStringMenu();
    }
    public static void validInputString() {
        LuhnAlgotirhm validateInputString = new LuhnAlgotirhm();
        System.out.println("Input " + inputStringWithOutLastDigit + " " +lastDigit);
        System.out.println("Provided: " + validateInputString.getInputLastDigit());
        System.out.println("exp. last digit: "+ validateInputString.getExpectedLastDigit());
        System.out.println("Checksum: " +"Valid");
        System.out.println("Digits: "+ inputString.length());
        System.out.println();
        inputStringMenu();
    }
    public static void inValidInputString() {
        LuhnAlgotirhm validateInputString = new LuhnAlgotirhm();
        System.out.println("Input " + inputStringWithOutLastDigit + " " +lastDigit);
        System.out.println("Provided: " + validateInputString.getInputLastDigit());
        System.out.println("exp. last digit: "+ validateInputString.getExpectedLastDigit());
        System.out.println("Checksum: " +"Invalid");
        System.out.println("Digits: "+ inputString.length());
        System.out.println();
        inputStringMenu();
    }
    public static void inputStringMenu() {
        sc = new Scanner(System.in);
        System.out.println("Write in the credit card number that you wanna search for");
        while (!sc.hasNextLine()) {
            System.out.println("Wrong input");
            sc.nextLine();
            inputStringMenu();
        }
        inputString = sc.nextLine();
        inputStringWithOutLastDigit = inputString.substring(0, inputString.length()-1);
        lastDigit = inputString.substring(inputString.length()-1,inputString.length());
        if(inputString.length() != 16) {
            isCreditCard = false;
        }

        LuhnAlgotirhm validateInputString = new LuhnAlgotirhm();
        if(validateInputString.validate(inputString)) {
            if(isCreditCard) {
                validCreditCardInputString();
            } else {
                validInputString();
            }
        }else {
            inValidInputString();
        }
    }

}
