package se.experis;

/*
* This program will perform the Luhn algorithm
 */

public class LuhnAlgotirhm {
    // Returns the expected that digit in the string
    public static int getExpectedLastDigit() {
        return expectedLastDigitInteger;
    }
    // Returns the last digit in the input string
    public int getInputLastDigit(){
        return inputLastDigitInteger;
    }

    public static int expectedLastDigitInteger = -1;
    public static int inputLastDigitInteger = -1;

    public LuhnAlgotirhm() {

    }
    /*
    The method will return true of the input string is valid or false if the input string is invalid
     */
    public boolean validate(String creditCardNumber) {

        inputLastDigitInteger = Integer.parseInt(creditCardNumber.substring(creditCardNumber.length()-1, creditCardNumber.length()));
        expectedLastDigitInteger = validateCreditCardDigit(creditCardNumber);
        if(expectedLastDigitInteger == inputLastDigitInteger) {
            if(validateCreditCardNumber(creditCardNumber)) {
                return true;
            }else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    /*
    * This method will validate the last digit in the input string and will return it
     */
    public int validateCreditCardDigit(String creditCardNumber) {

        String inputStringExceptTheLastDigit = creditCardNumber.substring(0,creditCardNumber.length()-1);
        // Array to save the digit in order to sum them
        int[] digitsArray = new int [inputStringExceptTheLastDigit.length()];
        // populate the digitArray
        for(int i = 0; i < inputStringExceptTheLastDigit.length(); i++) {
            digitsArray[i] = Integer.parseInt(inputStringExceptTheLastDigit.substring(i,i+1));
        }
        // this loop beguns from the end and will take out every second digit in the digit array
        // Every second digit is calculated and is then put back in the digit array
        for (int i = digitsArray.length - 1; i >= 0; i = i - 2) {
            int everySecondDigitInDigitArray = digitsArray[i];
            everySecondDigitInDigitArray = everySecondDigitInDigitArray * 2;

            if (everySecondDigitInDigitArray > 9) {

                everySecondDigitInDigitArray = everySecondDigitInDigitArray % 10 + 1;
            }
            digitsArray[i] = everySecondDigitInDigitArray;
        }
        // calculate the sum for the digit array
        int sumInputString = 0;
        for (int i = 0; i < digitsArray.length; i++) {

            sumInputString += digitsArray[i];
        }
        // calculate the last digit and return it
        int expectedLastDigit = sumInputString % 10;
        return 10-expectedLastDigit;
    }
    /*
     * This method will validate the the input string
     */
    public boolean validateCreditCardNumber(String creditCardNumber) {
        int[] digitsArray = new int [creditCardNumber.length()];
        // populate the digitArray
        for(int i = 0; i < creditCardNumber.length(); i++) {
            digitsArray[i] = Integer.parseInt(creditCardNumber.substring(i,i+1));
        }
        // this loop beguns from the end and will take out every second digit in the digit array
        // Every second digit is calculated and is then put back in the digit array
        for (int i = digitsArray.length - 2; i >= 0; i = i - 2) {
            int everySecondDigitInDigitArray = digitsArray[i];
            everySecondDigitInDigitArray = everySecondDigitInDigitArray * 2;

            if (everySecondDigitInDigitArray > 9) {

                everySecondDigitInDigitArray = everySecondDigitInDigitArray % 10 + 1;
            }
            digitsArray[i] = everySecondDigitInDigitArray;
        }
        int sumInputString = 0;
        for (int i = 0; i < digitsArray.length; i++) {

            sumInputString += digitsArray[i];
        }
        // check the sum
        int expectedLastDigit = sumInputString % 10;
        if (expectedLastDigit == 0) {
            return true;
        }else {
            return false;
        }
    }
}
